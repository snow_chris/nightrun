﻿using UnityEngine;
using System.Collections;

public class PlatformGenerator : MonoBehaviour {

    public Transform generationPoint;
    public float distanceBetweenMin;
    public float distanceBetweenMax;
    public ObjectPooler[] theObjectPools;

    private float[] platformWidths;
    private float distanceBetween;
    private int platformSelector;
    
    // Use this for initialization
    void Start () {
        platformWidths = new float[theObjectPools.Length];
        for (int i = 0; i < theObjectPools.Length; i++)
        {
            platformWidths[i] = theObjectPools[i].pooledObject.GetComponent<BoxCollider2D>().size.x / 2.0f;
        }
    }

    public void Init()
    {
        if (theObjectPools == null)
        {
            return;
        }

        for (int i = 0; i < theObjectPools.Length; i++)
        {
            theObjectPools[i].PoolReset();
        }
    }

    // Update is called once per frame
    void Update () {
	    if (transform.position.x < generationPoint.position.x)
        {
            distanceBetween = Random.Range(distanceBetweenMin, distanceBetweenMax);
            platformSelector = Random.Range(0, theObjectPools.Length);
            
            transform.position = new Vector3(transform.position.x + platformWidths[platformSelector] + distanceBetween, transform.position.y, transform.position.z);

            GameObject newPlatform = theObjectPools[platformSelector].GetPooledObject();
            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);

            transform.position = new Vector3(transform.position.x + platformWidths[platformSelector], transform.position.y, transform.position.z);
        } 
	}
}
