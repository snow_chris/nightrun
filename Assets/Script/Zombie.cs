﻿using UnityEngine;
using System.Collections;

public class Zombie : MonoBehaviour {

    public int HP;
    public bool isGrounded;
    public bool isAttack;
    public float moveSpeed;
    public float groundCheckRadius;
    public float delayTime;
    public LayerMask whatIsGround;
    public Transform groundCheck;
    public ZombieState state;
    public enum ZombieState : int { Idle, Attack };

    private Rigidbody2D rb;
    private Animator anim;

    // Use this for initialization
    void Start()
    {
        init();
    }

    public void init()
    {
        CancelInvoke();
        if (Time.timeScale == 0)
        {
            return;
        }
        
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        gameObject.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);
        state = (ZombieState)Random.Range(0, 2);
        isGrounded = false;
        isAttack = false;

        switch (state)
        {
            case ZombieState.Idle:
                Invoke("Idle", delayTime);
                break;
            case ZombieState.Attack:
                Invoke("Attack", delayTime);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }
        rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);

        anim.SetFloat("Speed", -rb.velocity.x);
        anim.SetBool("isAttack", isAttack);
        anim.SetInteger("HP", HP);
    }

    void Attack()
    {
        isAttack = true;
    }

    void AttackFinished()
    {
        isAttack = false;
        Invoke("Attack", delayTime);
    }

    void Idle()
    {
        moveSpeed = 0.0f;
    }

    void Dead()
    {
        gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), other.collider, true);
        }
    }
}
