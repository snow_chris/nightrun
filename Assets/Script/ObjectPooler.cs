﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour {

    public GameObject pooledObject;
    public int pooledAmount;
    private int pooledAmountStore;

    List<GameObject> pooledObjects;    

	// Use this for initialization
	void Start () {
        pooledObjects = new List<GameObject>();
        pooledAmountStore = pooledAmount;
        for (int i = 0; i < pooledAmount; i++)
        {
            pooledObjects.Add(InstantiateObject());
        }
	}

    public void PoolReset()
    {
        if (pooledObjects == null)
        {
            return;
        }

        for (int i = pooledAmountStore; i < pooledObjects.Count; i++)
        {
            Destroy(pooledObjects[i].gameObject);
        }
        pooledObjects.RemoveRange(pooledAmountStore, pooledObjects.Count - pooledAmountStore);

        for (int i = 0; i < pooledObjects.Count; i++)
        {
            pooledObjects[i].gameObject.SetActive(false);
        }
    }

    public GameObject GetPooledObject()
    {
        if (pooledObjects == null)
        {
            return null;
        }

        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeSelf)
            {
                return pooledObjects[i];
            }
        }

        GameObject obj = InstantiateObject();
        pooledObjects.Add(obj);
        return obj;
    }

    public GameObject InstantiateObject()
    {
        GameObject obj = (GameObject)Instantiate(pooledObject);
        obj.SetActive(false);

        return obj;
    }
}
