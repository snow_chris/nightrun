﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScrollingBackground : MonoBehaviour {

    public float parallaxSpeed;
    public Transform cameraTransform;
    public Rigidbody2D rbPlayer;

    private Transform[] layers;
    private Rigidbody2D rb;
    private float backgroundSize;
    private List<int> queue;

	// Use this for initialization
	void Start ()
    {
        queue = new List<int>();
        layers = new Transform[transform.childCount];
        for (int i = 0; i < layers.Length; i++)
        {
            layers[i] = transform.GetChild(i);
            queue.Add(i);
        }
        
        backgroundSize = (float)layers[0].GetComponent<SpriteRenderer>().bounds.size.x;
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {   
        ScrollRight(Time.deltaTime);
        rb.velocity = new Vector2(rbPlayer.velocity.x + parallaxSpeed, rb.velocity.y);
        if (!rbPlayer.gameObject.activeSelf)
        {
            rb.velocity = Vector2.zero;
        }
    }

    private void ScrollRight(float deltaTime)
    {
        int leftIndex = queue[0];
        int rightIndex = queue[2];
        float leftPosition = layers[leftIndex].transform.position.x;
        float rightPosition = layers[rightIndex].transform.position.x;
        float cameraPosition = cameraTransform.position.x;

        if (cameraPosition <= leftPosition)
        {
            queue.RemoveAt(queue.Count - 1);
            queue.Insert(0, rightIndex);
            layers[rightIndex].transform.position -= Vector3.right * backgroundSize * 3;
        }
        else if (cameraPosition >= rightPosition)
        {
            queue.RemoveAt(0);
            queue.Add(leftIndex);
            layers[leftIndex].transform.position += Vector3.right * backgroundSize * 3;
        }
    }
}
