﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public int HP;
    public bool isGrounded;
    public float moveSpeed;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    public Transform thePlayer;
    public Transform groundCheck;
    
    protected Rigidbody2D rb;
    protected Animator anim;  

	// Use this for initialization
	void Start ()
    {
        Physics2D.IgnoreCollision(thePlayer.GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);
	}
	
	// Update is called once per frame
	void Update ()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        anim.SetFloat("Speed", rb.velocity.x);
        anim.SetBool("IsGrounded", isGrounded);
        anim.SetInteger("HP", HP);
    }
}
