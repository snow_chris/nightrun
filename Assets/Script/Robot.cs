﻿using UnityEngine;
using System.Collections;

public class Robot : MonoBehaviour {

    public int HP;
    public bool isGrounded;
    public bool isAttack;
    public bool isShooting;
    public bool isSliding;
    public float moveSpeed;
    public float slideSpeed;
    public float jumpForce;
    public float groundCheckRadius;
    public float delayTime;
    public LayerMask whatIsGround;
    public Transform groundCheck;
    public RobotState state;
    public enum RobotState : int { Idle, Attack, Shoot, Jump, Slide };

    private Rigidbody2D rb;
    private Animator anim;

    // Use this for initialization
    void Start()
    {
        init();
    }

    public void init()
    {
        CancelInvoke();
        if (Time.timeScale == 0)
        {
            return;
        }
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        gameObject.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);

        isGrounded = false;
        isAttack = false;
        isShooting = false;
        isSliding = false;
        state = (RobotState)Random.Range(0, 5);
        switch (state)
        {
            case RobotState.Idle:
                Invoke("Idle", delayTime);
                break;
            case RobotState.Attack:
                Invoke("Attack", delayTime);
                break;
            case RobotState.Jump:
                Invoke("Jump", delayTime);
                break;
            case RobotState.Shoot:
                Invoke("Shoot", delayTime);
                break;
            case RobotState.Slide:
                Invoke("Slide", delayTime);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }
        rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);

        anim.SetFloat("Speed", -rb.velocity.x);
        anim.SetBool("isGrounded", isGrounded);
        anim.SetBool("isShooting", isShooting);
        anim.SetBool("isAttack", isAttack);
        anim.SetBool("isSliding", isSliding);
        anim.SetInteger("HP", HP);
    }

    void Attack()
    {
        isAttack = true;
    }
    void AttackEnd()
    {
        isAttack = false;
        Invoke("Attack", delayTime);
    }

    void Idle()
    {
        moveSpeed = 0.0f;
    }

    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        Invoke("Jump", delayTime);
    }

    void Shoot()
    {
        isShooting = true;
    }
    void ShootEnd()
    {
        isShooting = false;
        Invoke("Shoot", delayTime);
    }

    void Slide()
    {
        moveSpeed += slideSpeed;
        isSliding = true;
    }
    void SlideEnd()
    {
        moveSpeed -= slideSpeed;
        isSliding = false;
        Invoke("Slide", delayTime);
    }

    void Dead()
    {
        gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), other.collider, true);
        }
    }
}
