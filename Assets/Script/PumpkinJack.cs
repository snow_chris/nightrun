﻿using UnityEngine;
using System.Collections;

public class PumpkinJack : MonoBehaviour {

    public int HP;
    public bool isGrounded;
    public bool isSliding;
    public float moveSpeed;
    public float slidingSpeed;
    public float jumpForce;
    public float groundCheckRadius;
    public float delayTime;
    public LayerMask whatIsGround;
    public Transform groundCheck;
    public PumpkinState state;
    public enum PumpkinState : int { Idle, Jump, Slide };

    private Rigidbody2D rb;
    private Animator anim;

    // Use this for initialization
    void Start()
    {
        init();
    }

    public void init()
    {
        CancelInvoke();
        if (Time.timeScale == 0)
        {
            return;
        }
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        gameObject.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);

        isGrounded = false;
        isSliding = false;
        state = (PumpkinState)Random.Range(0, 3);
        switch (state)
        {
            case PumpkinState.Idle:
                Invoke("Idle", delayTime);
                break;
            case PumpkinState.Jump:
                Invoke("Jump", delayTime);
                break;
            case PumpkinState.Slide:
                Invoke("Slide", delayTime);
                break;
        }

        anim.SetFloat("Speed", -rb.velocity.x);
        anim.SetBool("isGrounded", isGrounded);
        anim.SetBool("isSliding", isSliding);
        anim.SetInteger("HP", HP);
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }

        rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);

        anim.SetFloat("Speed", -rb.velocity.x);
        anim.SetBool("isGrounded", isGrounded);
        anim.SetBool("isSliding", isSliding);
        anim.SetInteger("HP", HP);
    }

    void Idle()
    {
        moveSpeed = 0.0f;
    }

    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        Invoke("Jump", delayTime);
    }

    void Slide()
    {
        isSliding = true;
        moveSpeed += slidingSpeed;
    }
    void SlideEnd()
    {
        moveSpeed -= slidingSpeed;
        isSliding = false;
        Invoke("Slide", delayTime);
    }

    public void Dead()
    {
        gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), other.collider, true);
        }
    }
}
