﻿using UnityEngine;
using System.Collections;

public class EnemyGenerator : MonoBehaviour {
    public float minGenerateTime;
    public float maxGenerateTime;
    public ObjectPooler[] theObjectPools;
    
    void Start()
    {
        Generate();
    }

    void Update()
    {
        
    }

    public void Init()
    {
        CancelInvoke();
        for (int i = 0; i < theObjectPools.Length; i++)
        {
            theObjectPools[i].PoolReset();
        }
        Generate();
    }

    public void Generate()
    {
        int platformSelector = Random.Range(0, theObjectPools.Length);
        float nextTime = Random.Range(minGenerateTime, maxGenerateTime);

        GameObject newPlatform = theObjectPools[platformSelector].GetPooledObject();
        if (newPlatform == null)
        {
            Invoke("Generate", nextTime);
            return;
        }
        newPlatform.transform.position = transform.position;
        newPlatform.transform.rotation = transform.rotation;
        newPlatform.SetActive(true);

        switch (platformSelector)
        {
            case 0:
                newPlatform.GetComponent<Ninja>().init();
                break;
            case 1:
                newPlatform.GetComponent<PumpkinJack>().init();
                break;
            case 2:
                newPlatform.GetComponent<Zombie>().init();
                break;
            case 3:
                newPlatform.GetComponent<Robot>().init();
                break;
        }
        
        Invoke("Generate", nextTime);
    }
}
