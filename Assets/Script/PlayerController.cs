﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public int HP;
    public float slideSpeed;
    public float moveSpeed;
    public float speedMultiplier;
    public float speedIncreaseMilestone;
    public float jumpForce;
    public float jumpTime;
    public float HitTime;
    public bool isGrounded;
    public bool isHit;
    public LayerMask whatIsGround;
    public Transform groundCheck;
    public float groundCheckRadius;
    public GameManager theGameManager;
    
    private Rigidbody2D rb;
    private Animator anim;
    private Collision2D enemyCollision; 
    private bool stoppedJumping;
    private bool isSliding;
    private int hpStore;
    private float jumpTimeCounter;
    private float speedMilestoneCount;
    private float speedMilestoneCountStore;
    private float moveSpeedStore;
    private float speedIncreaseMilestoneStore;
    private CameraControl cameraControl;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        cameraControl = Camera.main.GetComponent<CameraControl>();
        //gameObject.transform.position = new Vector3(Camera.main.GetComponent<Transform>().position.x - 6, gameObject.transform.position.y, gameObject.transform.position.z);
        isGrounded = false;
        isHit = false;
        isSliding = false;
        stoppedJumping = true;
        hpStore = HP;
        jumpTimeCounter = jumpTime;
        speedMilestoneCount = speedIncreaseMilestone;
        moveSpeedStore = moveSpeed;
        speedMilestoneCountStore = speedMilestoneCount;
        speedIncreaseMilestoneStore = speedIncreaseMilestone;
    }
	
	// Update is called once per frame
	void Update () {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        if (transform.position.x > speedMilestoneCount)
        {
            speedMilestoneCount += speedIncreaseMilestone;
            speedIncreaseMilestone = speedIncreaseMilestone * speedMultiplier;
            moveSpeed *= speedMultiplier;
        }

        float speed = moveSpeed + (isSliding ? slideSpeed : 0);
        rb.velocity = new Vector2(speed, rb.velocity.y);

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (isGrounded)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                stoppedJumping = false;
            }
        }

        if (Input.GetKey(KeyCode.UpArrow) && !stoppedJumping)
        {
            if (jumpTimeCounter > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                jumpTimeCounter -= Time.deltaTime;
            }
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            jumpTimeCounter = 0;
            stoppedJumping = true;
        }

        if (isGrounded)
        {
            jumpTimeCounter = jumpTime;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (isGrounded)
            {
                Slide();
            }
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            SlideEnd();
        }

        anim.SetFloat("Speed", rb.velocity.x);
        anim.SetBool("IsGrounded", isGrounded);
        anim.SetBool("isSliding", isSliding);
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Destroy")
        {
            moveSpeed = moveSpeedStore;
            theGameManager.RestartGame();
            speedMilestoneCount = speedMilestoneCountStore;
            speedIncreaseMilestone = speedIncreaseMilestoneStore;
        }

        if (other.gameObject.tag == "Enemy")
        {
            Hit(other);
        }
    }

    void Hit(Collision2D other)
    {
        HP--;
        if (HP <= 0)
        {
            HP = hpStore;
            moveSpeed = moveSpeedStore;
            theGameManager.RestartGame();
            speedMilestoneCount = speedMilestoneCountStore;
            speedIncreaseMilestone = speedIncreaseMilestoneStore;
        }
        else
        {
            enemyCollision = other;
            Physics2D.IgnoreCollision(enemyCollision.collider, gameObject.GetComponent<BoxCollider2D>(), true);
            cameraControl.Shake(0.5f, 4, 10);
            Invoke("HitFinished", HitTime);
        }
    }

    void HitFinished()
    {
        Physics2D.IgnoreCollision(enemyCollision.collider, gameObject.GetComponent<BoxCollider2D>(), false);
    }

    void Shoot()
    {
        anim.SetBool("isShooting", true);
    }

    void ShootEnd()
    {
        anim.SetBool("isShooting", false);
    }

    void Slide()
    {
        isSliding = true;
    }
    void SlideEnd()
    {
        isSliding = false;
    }
}
