﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Transform platformGenerator;
    public PlayerController thePlayer;
    public DeathMenu theDeathScreen;
    public Text theScoreText;
    public Image[] Heart;
    public EnemyGenerator theEnemyGenerator;
    public PlatformGenerator thePlatformGenerator;

    private Vector3 platformStartPoint;
    private Vector3 playerStartPoint;
    private PlatformDestroyer[] platformList;
    private float score;

	// Use this for initialization
	void Start () {
        Screen.SetResolution(1600, 900, false);
        platformStartPoint = platformGenerator.position;
        playerStartPoint = thePlayer.transform.position;
        score = 0.0f;
        theScoreText.text = "Score : 0";
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeScale == 0.0f || theDeathScreen.gameObject.activeSelf)
        {
            return;
        }
        int hp = thePlayer.HP;
        for (int i = hp; i < Heart.Length; i++)
        {
            Heart[i].gameObject.GetComponent<Image>().color = Color.clear;
        }
        score += Time.deltaTime * thePlayer.moveSpeed * 2.0f;
        theScoreText.text = string.Format("Score : {0}", (int)score);
    }

    public void RestartGame()
    {
        Time.timeScale = 0;
        thePlayer.gameObject.SetActive(false);
        theDeathScreen.gameObject.SetActive(true);
        theDeathScreen.theScoreText.text = ((int)score).ToString();
        theEnemyGenerator.Init();
    }

    public void Reset()
    {
        platformList = FindObjectsOfType<PlatformDestroyer>();
        for (int i = 0; i < platformList.Length; i++)
        {
            platformList[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < Heart.Length; i++)
        {
            Heart[i].gameObject.GetComponent<Image>().color = Color.white;
        }
        thePlayer.transform.position = playerStartPoint;
        platformGenerator.position = platformStartPoint;
        thePlayer.gameObject.SetActive(true);
        theDeathScreen.gameObject.SetActive(false);
        theEnemyGenerator.Init();
        thePlatformGenerator.Init();
        score = 0.0f;
    }
}
