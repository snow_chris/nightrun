﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public string playGameScene;

    private void Start()
    {
        Screen.SetResolution(1600, 900, false);
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(playGameScene);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
