﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public string mainMenuScene;
    public Transform canvas;
    public Transform theDeathMenu;

    public void PauseGame()
    {
        if (!theDeathMenu.gameObject.activeSelf)
        {
            Time.timeScale = 0;
            canvas.gameObject.SetActive(true);
        }
    }

    public void QuitToMain()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(mainMenuScene);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        canvas.gameObject.SetActive(false);
    }
}
