﻿using UnityEngine;
using System.Collections;

public class Ninja : MonoBehaviour
{
    public int HP;
    public bool isGrounded;
    public bool isGlide;
    public bool isAttack;
    public bool isThrowing;
    public bool isSliding;
    public float moveSpeed;
    public float slideSpeed;
    public float jumpForce;
    public float groundCheckRadius;
    public float delayTime;
    public float GlideForce;
    public LayerMask whatIsGround;
    public Transform groundCheck;
    public NinjaState state;
    public enum NinjaState : int { Idle, Attack, Glide, Jump, Throw, Slide };
    
    private Rigidbody2D rb;
    private Animator anim;
    
    // Use this for initialization
    void Start()
    {
        init();
    }

    public void init()
    {
        CancelInvoke();
        if (Time.timeScale == 0)
        {
            return;
        }
        
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        gameObject.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);

        isGrounded = false;
        isGlide = false;
        isAttack = false;
        isThrowing = false;
        isSliding = false;

        state = (NinjaState)Random.Range(0, 6);
        switch (state)
        {
            case NinjaState.Idle:
                Invoke("Idle", delayTime);
                break;
            case NinjaState.Attack:
                Invoke("Attack", delayTime);
                break;
            case NinjaState.Glide:
                Invoke("Glide", delayTime);
                break;
            case NinjaState.Jump:
                Invoke("Jump", delayTime);
                break;
            case NinjaState.Throw:
                Invoke("Throw", delayTime);
                break;
            case NinjaState.Slide:
                Invoke("Slide", delayTime);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }
        if (isGlide)
        {
            rb.velocity = new Vector2(-moveSpeed, GlideForce);
        }
        else
        {
            rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);
        }

        anim.SetFloat("Speed", -rb.velocity.x);
        anim.SetBool("isGrounded", isGrounded);
        anim.SetBool("isGliding", isGlide);
        anim.SetBool("isThrowing", isThrowing);
        anim.SetBool("isAttack", isAttack);
        anim.SetBool("isSliding", isSliding);
        anim.SetInteger("HP", HP);
    }

    void Glide()
    {
        isGlide = true;
        moveSpeed = 0.0f;
    }

    void Attack()
    {
        isAttack = true;
    }
    void AttackEnd()
    {
        isAttack = false;
        Invoke("Attack", delayTime);
    }

    void Idle()
    {
        moveSpeed = 0.0f;
    }

    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        Invoke("Jump", delayTime);
    }

    void Throw()
    {
        isThrowing = true;
    }
    void ThrowEnd()
    {
        isThrowing = false;
        Invoke("Throw", delayTime);
    }

    void Slide()
    {
        moveSpeed += slideSpeed;
        isSliding = true;
    }
    void SlideEnd()
    {
        moveSpeed -= slideSpeed;
        isSliding = false;
        Invoke("Slide", delayTime);
    }

    void Dead()
    {
        gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), other.collider, true);
        }
    }
}
